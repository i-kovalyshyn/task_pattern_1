package com.epam.enums;

public enum PizzaType {
    CheesePizza, ClamPizza, VeggiePizza
}
