package com.epam.model;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PizzaComponents {
    private String dough;

    private String sauce;

    private String toppings;

    private double price;
}
